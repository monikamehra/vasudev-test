let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let Userschema = new Schema({
    first_name: {
        type: String,
        index: true,
        default: ""
    },
    last_name: {
        type: String,
        index: true,
        default: ""
    },
    contact: {
        type: String,
        default: "",
        index: true
    },
    profile_picture: {
        original: { type: String, default: '' },
        thumbnail: { type: String, default: '' }
    },
    geometry: {
        type: [Number], index: '2dsphere', sparse: true
    },
    address: {
        type: String,
        default: ""
    },
    gender: {
        type: String,
        enum: ['Male', 'Female', 'Others' ],
        default: 'Male'
    }
}, { timestamps: true });

const user = mongoose.model('users', Userschema);
module.exports = user;
