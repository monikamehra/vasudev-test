const AppConstraints  = require('../config/appConstants');
const responseMessage = require('../config/response-messages');

// exports.invalidAccessTokenError = function (statusCode, language, message, res) {
exports.invalidAccessTokenError = function (statusCode, message, res) {
    // let lang = language ? language : 'en';
    var successData = {
        status: AppConstraints.STATUSCODE.UNAUTHORIZE,
        data: {},
        // message: message.message ? message.message[lang] : responseMessage.STATUS_MSG.ERROR.DEFAULT.message[lang]
        message:  responseMessage.STATUS_MSG.ERROR.DEFAULT.message
    };
    sendData(successData, statusCode, res);
};

// exports.somethingWentWrongError = function (statusCode, language, message, res) {
exports.somethingWentWrongError = function (statusCode, message, res) {

    // let lang = language ? language : 'en';
    var successData = {
        status: AppConstraints.STATUSCODE.INTERNAL_SERVER_ERROR,
        data: {},
        message:  responseMessage.STATUS_MSG.ERROR.DEFAULT.message
        // message: message.message ? message.message[lang] : responseMessage.STATUS_MSG.ERROR.DEFAULT.message[lang]
    };
    sendData(successData, statusCode, res);
};


// exports.accountBlockedOrDeleted = function (statusCode, language, message, res) {
exports.accountBlockedOrDeleted = function (statusCode, message, res) {

    // let lang = language ? language : 'en';
    var successData = {
        status: (statusCode === 402 ? AppConstraints.STATUSCODE.APP_ERROR : AppConstraints.STATUSCODE.ROLE_CHANGE),
        data: {},
        message:  responseMessage.STATUS_MSG.ERROR.DEFAULT.message
        // message: message.message ? message.message[lang] : responseMessage.STATUS_MSG.ERROR.DEFAULT.message[lang]
    };
    sendData(successData, statusCode, res);
};



// exports.sendSuccessData = async function (data, statusCode, language, message, res, decryptColumns = []) {
exports.sendSuccessData = async function (data, statusCode, message, res, decryptColumns = []) {

    // let lang = language ? language : 'en';

    var successData = {
        status: 1,
        statusCode: statusCode,
        data: data,
        // message:  message.message ? message.message[lang] : responseMessage.STATUS_MSG.ERROR.DEFAULT.message[lang]
        message:  responseMessage.STATUS_MSG.ERROR.DEFAULT.message
    };
    sendData(successData, statusCode, res);
};
// exports.sendErrorMessage = function (statusCode, language, message, res) {
exports.sendErrorMessage = function (statusCode, message, res) {

    // let lang = language ? language : 'en';
    var successData = {
        status: 0,
        statusCode: statusCode,
        data: {},
        // message: message.message ? message.message[lang] : responseMessage.STATUS_MSG.ERROR.DEFAULT.message[lang]
        message:  responseMessage.STATUS_MSG.ERROR.DEFAULT.message

    };
    sendData(successData, statusCode, res);
};


// exports.sendErrorMessageData = async function (statusCode, language, message, data, res) {
exports.sendErrorMessageData = async function (statusCode, message, data, res) {

    // let lang = language ? language : 'en';
    var successData = {
        status: 0,
        statusCode: statusCode,
        data: data,
        message:  responseMessage.STATUS_MSG.ERROR.DEFAULT.message
        // message: message.message ? message.message[lang] : responseMessage.STATUS_MSG.ERROR.DEFAULT.message[lang]
    };
    sendData(successData, statusCode, res);
};


// exports.sendError = function (statusCode, error, language, res) {
exports.sendError = function (statusCode, error, res) {

    // let lang = language ? language : 'en';
    var errorMessage = {
        statusCode:statusCode,
        data: {},
        message: error
    };
    sendData(errorMessage, statusCode, res);
};


exports.successStatusMsg = function (res) {

    var successMsg = {"status": "true"};
    sendData(successMsg, res);
};


async function sendData(data, statusCode, res) {
    // console.log(res,'====res==========');
    
    return res.status(statusCode).json(data);
}


exports.sendData = async function (data, statusCode, res) {
    return res.status(statusCode).json(data);
};