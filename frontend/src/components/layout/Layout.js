import React, {useState} from "react";
import { connect } from 'react-redux';
import Helmet from 'react-helmet';


const Layout = (props) => {
   
   
    return(
        <body className="hold-transition sidebar-mini layout-fixed">
            <Helmet titleTemplate={`%s | Test`} /> 
            <div className="wrapper">
         
            <div className="content-wrapper">
                <section className="content">
                    {React.cloneElement(props.children)}
                </section>
            </div>
            </div>
        </body>
    )
}

const mapStateToPros = (state, props) => {
    return{
        isAuthenticated: state.Auth.isAuthenticated,
        user: state.Auth,
    }
};

export default connect(
  mapStateToPros,
)(Layout);



