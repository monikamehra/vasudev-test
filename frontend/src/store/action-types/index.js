//auth
export const AUTH_LOGIN = 'AUTH_LOGIN';
export const AUTH_CHECK = 'AUTH_CHECK';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';
export const GET_LIST = 'GET_LIST';
export const DELETE_DATA = 'DELETE_DATA';
export const GET_DETAIL = 'GET_DETAIL';
export const PERSIST_STORE = 'persist/REHYDRATE';
