import React, {useState, useEffect} from "react";
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import * as Path from 'routes/paths';
import AuthService from 'services';
import swal from 'sweetalert';
import env from 'env.json';
import { Link } from 'react-router-dom';
import ContentLoader  from 'react-content-loader';

const UserView = (props) => {
    
    const [sending, setSending] = useState(false);
    const [data, setData] = useState({});
    const MODULE_NAME = 'user';

    //get user
    let isMounted = true;
    async function getData() {
        try{
            let query = {
                id: props.match.params.id,
                MODULE_NAME
            };
            setSending(true);
            await props.dispatch(AuthService.getDetail(query)).then((res) => {
                if(isMounted){
                    setSending(false);
                    setData(res.body);
                }
            });
        }catch(err) {
            setSending(false)
            console.log(err);
            if(err && err.data && err.data.message){
                swal("Oops!", err.data.message, "error");
            }
        }
    };
  
    useEffect(() => {
        getData();
        return () => {
            isMounted = false;
        };
    }, []);

    return(
        <>
        <Helmet title="Users" />
            <section className="content-header">
                <div className="container-fluid">
                    <div className="row mb-2">
                        <div className="col-sm-6">
                            <h1>User Detail</h1>
                        </div>
                    </div>
                </div>
            </section>

            {sending && 
                <ContentLoader
                height={200}
                style={{ padding: 10, }} 
                speed={1}
                backgroundColor={'#f3f3f3'}
                foregroundColor={'#c1c5c7'}
                viewBox="0 0 380 70"
              >
                {/* Only SVG shapes */}
                <rect x="0" y="0" rx="5" ry="5" width="70" height="70" />
                <rect x="80" y="17" rx="4" ry="4" width="300" height="13" />
                <rect x="80" y="40" rx="3" ry="3" width="250" height="10" />
              </ContentLoader>
            }
            {(!sending && data)  &&
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <div className="card card-primary card-outline">
                                    <div className="card-body box-profile">
                                        <div className="text-center">
                                        <a href={data.profile_picture && data.profile_picture.original ? env.LOCAL_URL+data.profile_picture.original : '#'} target="_blank">
                                            <img className="profile-user-img img-fluid img-circle user-detail-img"
                                            src={data.profile_picture && data.profile_picture.original ? env.LOCAL_URL+data.profile_picture.original : '/assets/images/dummy.png'}
                                            alt="User profile picture" />
                                            </a>
                                        </div>

                                        {/* <h3 className="profile-username text-center">{data.first_name} {data.last_name}</h3> */}

                                        <ul className="list-group list-group-unbordered mb-3">
                                            <li className="list-group-item">
                                                <b>First Name</b> <span className="float-right">{data.first_name}</span>
                                            </li>
                                            <li className="list-group-item">
                                                <b>Last Name</b> <span className="float-right">{data.last_name}</span>
                                            </li>
                                            {(data.contact) &&
                                                <li className="list-group-item">
                                                    <b>Contact</b> <span className="float-right">{data.contact} </span>
                                                </li>
                                            }
                                            {data.address &&
                                                <li className="list-group-item">
                                                    <b>Address</b> <span className="float-right">{data.address}</span>
                                                </li>
                                            }
                                            <li className="list-group-item">
                                                <b>Gender</b> <span className="float-right">{data.gender}</span>
                                            </li>
                                        </ul>

                                        <Link to={Path.user} class="btn btn-primary btn-block"><b>Back</b></Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>   
            }
        </>
    )
};

const mapStateToProps = (state) => {
    return{
        isAuthenticated: state.Auth.isAuthenticated,
        user: state.Auth,
    }
};

function mapDispatchToProps(dispatch) {
    return { dispatch };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserView);

