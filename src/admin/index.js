/**
 * Makes all features available to outer modules.
 */

const express = require('express')
const router = express.Router();

router.use('/user', require('./user').Routes);

module.exports = {
    routes: [router],
};