'use strict';

const { Validator } = require('node-input-validator');
var jwt = require('jsonwebtoken');
const helper = require('../../../helper/helper');
const bcrypt = require('bcryptjs');
const Constants = require('../../../config/appConstants');
const Models = require('../../../models');

module.exports = {

    file_upload: async function (req, res, next) {
        try {
            let v = new Validator( req.body, {
                type: 'required|in:image',
                folder: 'required|in:user',
            });
            var errorsResponse
            await v.check().then(function (matched) {
                if (!matched) {
                    var valdErrors=v.errors;
                    var respErrors=[];
                    Object.keys(valdErrors).forEach(function(key)
                    {
                        if(valdErrors && valdErrors[key] && valdErrors[key].message){
                            respErrors.push(valdErrors[key].message);
                        }
                    });   
                    errorsResponse=respErrors.join(', ');
                }
            });
            console.log(errorsResponse,'---errorsResponse---');
            if(errorsResponse){
                return helper.failed(res, errorsResponse)
            }
            // return
            
            if (req.files == null) {
                return helper.failed(res, "Please select image")
            }

            console.log("req.body ------------ ", req.body)
            console.log("req.files ------------ ", req.files)
            let PAYLOAD = req.body;
            var FILE_TYPE = PAYLOAD.type; // image,video,etc
            var FOLDER = req.body.folder; //PAYLOAD.folder;// user,category,products,etc

            var image_data = [];
            if (req.files && req.files.image && Array.isArray(req.files.image)) {
                for (var imgkey in req.files.image) {
                    var image_url = await helper.fileUploadMultiparty(req.files.image[imgkey], FOLDER, FILE_TYPE);
                    image_data.push(image_url)
                }
                return res.status(200).json({
                    status: true,
                    code: 200,
                    message: 'Successufully',
                    body: image_data
                });
            } else if (req.files.image.name != "") {
                var image_url = await helper.fileUploadMultiparty(req.files.image, FOLDER, FILE_TYPE);
                image_data.push(image_url)
                return res.status(200).json({
                    status: true,
                    code: 200,
                    message: 'Successufully',
                    body: image_data
                });
            } else {
                return res.status(200).json({
                    status: false,
                    code: 400,
                    message: "Error - Image can't be empty",
                    body: []
                });
            }
        } catch (err) {
            console.log(err)
            return helper.failed(res, err)
        }
    },

    list: async (req, res) => {
		try {
			const PAGE_SIZE = parseInt(process.env.PAGE_SIZE);
			const page = parseInt(req.query.page || 1);
			var total = 0;

			let get_all_data = await Models.Users.find().limit(PAGE_SIZE).skip((page - 1) * PAGE_SIZE).sort({_id: -1});
            total = await Models.Users.countDocuments();
            
			if(get_all_data){
				let finalObj = {
					total_pages: Math.ceil(total / PAGE_SIZE),
					total_count:total,
					current_page:page,
					per_page:PAGE_SIZE,
					data : get_all_data
				}
				return helper.success(res, "All users List.", finalObj);
			} else {
				return helper.failed(res, "Invalid Data")
			}
		} catch (error) {
			return helper.failed(res, error)
		}
    },

	view: async (req, res) => {
		try {
			let v = new Validator( req.params, {
                id: 'required', 
            });
            var errorsResponse
            await v.check().then(function (matched) {
                if (!matched) {
                    var valdErrors=v.errors;
                    var respErrors=[];
                    Object.keys(valdErrors).forEach(function(key)
                    {
                        if(valdErrors && valdErrors[key] && valdErrors[key].message){
                            respErrors.push(valdErrors[key].message);
                        }
                    });   
                    errorsResponse=respErrors.join(', ');
                }
            });
            if(errorsResponse){
               return helper.failed(res, errorsResponse)
            }
            const _id = req.params.id;

			let get_detail = await Models.Users.findOne({ _id });
			if(get_detail){
				return helper.success(res, "User detail.", get_detail);
			} else {
				return helper.failed(res, "User not exists")
			}
		} catch (error) {
			console.log(error)
			return helper.failed(res, error)
		}
    },

    add: async (req, res) => {
		try {
            let v = new Validator( req.body, {
                first_name: 'required|string',
                last_name: 'required|string',
                contact: 'required|string',
                profile_picture: 'required',
                address: 'required|string',
                gender: 'required|in:Male,Female,Others'
            });
            var errorsResponse
            await v.check().then(function (matched) {
                if (!matched) {
                    var valdErrors=v.errors;
                    var respErrors=[];
                    Object.keys(valdErrors).forEach(function(key)
                    {
                        if(valdErrors && valdErrors[key] && valdErrors[key].message){
                            respErrors.push(valdErrors[key].message);
                        }
                    });   
                    errorsResponse=respErrors.join(', ');
                }
            });
            if(errorsResponse){
               return helper.failed(res, errorsResponse)
			}
			const {first_name, last_name, contact, profile_picture, address, gender } = req.body;

            const find_exist_phone = await Models.Users.findOne({ contact });
			if(find_exist_phone){
				return helper.failed(res, "This phone number already exists.")
            }

            //upload images
            let image_val = [];
			if (req.body.profile_picture) {
				for (var imgkey of JSON.parse(profile_picture)) {
					let image_data = {
						original : imgkey.image,
						thumbnail : imgkey.thumbnail
					};
                    req.body.profile_picture = image_data;
                    image_val.push(image_data)
				}
			} 
            req.body.geometry = ['76.7179', '30.7046' ];

            const addUser = new Models.Users(req.body);
            const userData = await addUser.save();
            if(userData){
			    return helper.success(res, "User added successfully.", userData);
            }else {
                return helper.failed(res, "Something went wrong");
            }
        } catch (err) {
            return helper.failed(res, err);
        }
    },

    update: async (req, res) => {
		try {
            let v = new Validator( req.body, {
                _id: 'required',
                first_name: 'required|string',
                last_name: 'required|string',
                contact: 'required|string',
                profile_picture: 'required',
                address: 'required|string',
                gender: 'required|in:Male,Female,Others'
            });
            var errorsResponse
            await v.check().then(function (matched) {
                if (!matched) {
                    var valdErrors=v.errors;
                    var respErrors=[];
                    Object.keys(valdErrors).forEach(function(key)
                    {
                        if(valdErrors && valdErrors[key] && valdErrors[key].message){
                            respErrors.push(valdErrors[key].message);
                        }
                    });   
                    errorsResponse=respErrors.join(', ');
                }
            });
            if(errorsResponse){
               return helper.failed(res, errorsResponse)
			}
			const {_id, first_name, last_name, contact, profile_picture, address, gender } = req.body;

            let checkUserExists = await Models.Users.findOne({ _id })

            if(!checkUserExists) {
                return helper.failed(res, "User is not exists!");
            }

            const find_exist_phone = await Models.Users.findOne({ contact, _id: { $ne: _id } });
			if(find_exist_phone){
				return helper.failed(res, "This phone number already exists.")
            }

            //upload images
            let image_val = [];
			if (req.body.profile_picture) {
				for (var imgkey of JSON.parse(profile_picture)) {
					let image_data = {
						original : imgkey.image,
						thumbnail : imgkey.thumbnail
					};
                    req.body.profile_picture = image_data;
                    image_val.push(image_data)
				}
			} 

            req.body.geometry = ['76.7179', '30.7046' ];

            let updateUser = await Models.Users.updateMany({_id}, req.body)
            if(updateUser){
                let getUser = await Models.Users.findOne({_id})
			    return helper.success(res, "User updated successfully.", getUser);
            }else {
                return helper.failed(res, "Something went wrong");
            }
        } catch (err) {
            return helper.failed(res, err);
        }
    },

	delete: async (req, res) => {
		try {
			let v = new Validator( req.params, {
                id: 'required', 
            });
            var errorsResponse
            await v.check().then(function (matched) {
                if (!matched) {
                    var valdErrors=v.errors;
                    var respErrors=[];
                    Object.keys(valdErrors).forEach(function(key)
                    {
                        if(valdErrors && valdErrors[key] && valdErrors[key].message){
                            respErrors.push(valdErrors[key].message);
                        }
                    });   
                    errorsResponse=respErrors.join(', ');
                }
            });
            if(errorsResponse){
               return helper.failed(res, errorsResponse)
            }
            const _id = req.params.id;

			let get_detail = await Models.Users.findOne({ _id });
			if(get_detail){
				await get_detail.remove();
				return helper.success(res, "User deleted successfully.", {});
			} else {
				return helper.failed(res, "User not exists!")
			}
		} catch (error) {
			console.log(error)
			return helper.failed(res, error)
		}
    },

};