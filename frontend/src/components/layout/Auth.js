import React from "react";
import Helmet from 'react-helmet';
import { connect } from 'react-redux';

const Auth = (props) => {
  return (
	  	<>
			<body className="hold-transition login-page">
				<Helmet titleTemplate={`%s | Test`} /> 
          {props.loader!=true &&
              <div className="preloader flex-column justify-content-center align-items-center">
                  <img className="animation__shake" src="/assets/images/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60" />
              </div>
          }
          {React.cloneElement(props.children)}
			</body>
		</>
  );
}

const mapStateToPros = (state) => {
    return{
        isAuthenticated: state.Auth.isAuthenticated,
        user: state.Auth,
        loader: state.persistStore.loader,
    }
};

export default connect(
  mapStateToPros,
)(Auth);

