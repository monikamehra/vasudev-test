import Http from 'Http';
import * as action from 'store/actions';

let adminApiPath = 'admin_api';

//user 
//list
export function getList(value) {
  let page = value.page;
  let sort = value.sortby;
  let search = value.search ? value.search : "";
  return (dispatch) => new Promise((resolve, reject) => {
    Http.get(`${adminApiPath}/${value.MODULE_NAME}/list?page=`+page)
      .then((res) => resolve(res.data))
      .catch((err) => {
        const { status, data } = err.response;
        const res = {
          status,
          data,
        };
        return reject(res);
      });
  });
}

//view
export function getDetail(value) {
  return (dispatch) => new Promise((resolve, reject) => {
    Http.get(`${adminApiPath}/${value.MODULE_NAME}/view/`+value.id)
      .then((res) => resolve(res.data))
      .catch((err) => {
        const { status, data } = err.response;
        const res = {
          status,
          data,
        };
        return reject(res);
      });
  });
}

//delete
export function deleteData(value) {
  return (dispatch) => new Promise((resolve, reject) => {
    Http.delete(`${adminApiPath}/${value.MODULE_NAME}/delete/`+value.id)
      .then((res) => resolve(res.data))
      .catch((err) => {
        const { status, data } = err.response;
        const res = {
          status,
          data,
        };
        return reject(res);
      });
  });
}