import * as ActionTypes from 'store/action-types';

export function authLogin(payload) {
  return {
    type: ActionTypes.AUTH_LOGIN,
    payload,
  };
}

export function authCheck() {
    return {
      type: ActionTypes.AUTH_CHECK,
    };
}

export function authLogout() {
  return {
    type: ActionTypes.AUTH_LOGOUT,
  };
}

export function getList(payload) {
  return {
    type: ActionTypes.GET_LIST,
    payload
  };
}

export function getDetail(payload) {
  return {
    type: ActionTypes.GET_DETAIL,
    payload
  };
}

export function deleteData(payload) {
  return {
    type: ActionTypes.DELETE_DATA,
    payload
  };
}

export function persist_store(payload) {
    return {
      type: ActionTypes.PERSIST_STORE,
        payload
    };
}