'use strict';

// npm modules
const express = require('express');

// router
const router = express.Router();

// local modules
const controller = require('./controller');

router.post('/file_upload', controller.file_upload);
router.post('/add', controller.add);
router.put('/update', controller.update);
router.get('/list', controller.list);
router.get('/view/:id', controller.view);
router.delete('/delete/:id', controller.delete);

module.exports = router;
