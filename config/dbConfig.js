'use strict';

let mongoURI = "";

if (process.env.NODE_ENV === "test") {
    mongoURI = ``;
} else if (process.env.NODE_ENV === "dev") {
    mongoURI = ``
    mongoURI = `mongodb://localhost:27017/vasudev_test_project`
} else if (process.env.NODE_ENV === "local") {
    mongoURI = `mongodb://localhost:27017/vasudev_test_project`;
}

module.exports = {
    mongo: mongoURI
};
