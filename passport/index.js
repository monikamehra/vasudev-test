const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;
const helper = require('../helper/helper');
const Models = require('../models');
const Constants     = require('../config/appConstants');

// Setup options for JWT Strategy
const jwtOptions = {};
jwtOptions.jwtFromRequest = ExtractJWT.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = Constants.SERVER.JWT_SECRET_KEY//jwtSecretKey;

// Create JWT Strategy
// module.exports = passport => {
    // for app api authentication
    passport.use('user', new JwtStrategy(jwtOptions, 
        async function(payload, done) {
            try {
                // console.log(payload, "payload");
                var criteria = {
                    _id: payload.data._id,
                    email: payload.data.email,
                    login_time: payload.data.login_time
                }
				Models.Users.findOne(criteria, function (err, result) {
                    if (err){
                        return done(null, err);
                    }else if (result) {
                        return done(null, result);
                    }else {
                        return done(null, false);
                    }
                });
            } catch(e) {
                console.log('not local');
                console.log(e);
                // return done(e, false);
            }
        }
    ));

    // for admin api authentication
    passport.use('admin', new JwtStrategy(jwtOptions, 
        async function(payload, done) {
            try {
                // console.log(payload, "payload");
                var criteria = {
                    _id: payload.data._id,
                    email: payload.data.email,
                    login_time: payload.data.login_time
                }
				Models.Users.findOne(criteria, function (err, result) {
                    if (err){
                        return done(null, err);
                    }else if (result) {
                        return done(null, result);
                    }else {
                        return done(null, false);
                    }
                });
            } catch(e) {
                console.log('not local');
                console.log(e);
                // return done(e, false);
            }
        }
    ));

module.exports = {
    initialize: function () {
        return passport.initialize();
    },
    
    authenticateUser: function (req, res, next) {
        console.log("coming here ===================")
        return passport.authenticate("user", {
            session: false
        }, (err, user, info) => {
			console.log(err, '=======================>passport err');
			// console.log(info, '=======================>passport info');
			// console.log(info && info['name'], '=======================>passport info[name]');
			// console.log(user, '=======================>passport err user');

            if (err) {
                return helper.error(res, err);
            }
            if (info && info.hasOwnProperty('name') && info.name == 'JsonWebTokenError') {
                return helper.error(res, {
					message: 'Invalid Token.'
				});
            } else if (user == false) {
                return helper.error(res, {
					message: 'Authorization is required'
				});
            }            
            // Forward user information to the next middleware
            req.user = user; 
            next();
        })(req, res, next);
    },

    authenticateAdmin: function (req, res, next) {
        console.log("coming here ===================")
        return passport.authenticate("admin", {
            session: false
        }, (err, admin, info) => {
			console.log(err, '=======================>passport err');
			// console.log(info, '=======================>passport info');
			// console.log(info && info['name'], '=======================>passport info[name]');
			// console.log(admin, '=======================>passport err admin');

            if (err) {
                return helper.error(res, err);
            }
            if (info && info.hasOwnProperty('name') && info.name == 'JsonWebTokenError') {
                return helper.error(res, {
					message: 'Invalid Token.'
				});
            } else if (admin == false) {
                return helper.error(res, {
					message: 'Authorization is required'
				});
            }            
            // Forward user information to the next middleware
            req.admin = admin; 
            next();
        })(req, res, next);
    },
};


