'use strict';

var STATUSCODE = {
    BAD_REQUEST: 400,
    INTERNAL_SERVER_ERROR: 500,
    SUCCESS: 200,
    UNAUTHORIZE: 401,
    CREATED: 201,
    APP_ERROR: 402,
    ROLE_CHANGE: 403
};


var SERVER = {
    APP_NAME: 'Vasudev Test',
    TOKEN_EXPIRATION: 60 * 60 * 24 * 30, // expires in 24 hours * 7 days 
    JWT_SECRET_KEY: 'vasudetestSecretKey',
    JWT_SECRET_KEY_ADMIN:'csrtgDgHDHVFVDhdwjbdhwghbGHSHDJSHD613SBHTBBDNAS',
    JWT_SECRET_VAL: 'vasudetest@12!!@1',
};

var MAILAUTH = {
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    // requireTLS: true,
    service: 'gmail',
    auth: {
      user: 'monikamehra635@gmail.com',
      pass: 'vasudevtest'
    }
};



var APP_CONSTANTS = {
    STATUSCODE: STATUSCODE,
    SERVER:SERVER,
    MAILAUTH:MAILAUTH,
};

module.exports = APP_CONSTANTS;