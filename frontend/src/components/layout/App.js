import React from "react";
import Helmet from 'react-helmet';
import { connect } from 'react-redux';


const App = (props) => {
  return (
	  	<>
			<div>
				<Helmet titleTemplate={`%s | Test`} /> 
				<div className="page-wrapper">
					{React.cloneElement(props.children)}
				</div>
		
			</div>
		</>
  );
}

const mapStateToPros = (state) => {
    return{
        isAuthenticated: state.Auth.isAuthenticated,
        user: state.Auth,
    }
};

export default connect(
  mapStateToPros,
)(App);


