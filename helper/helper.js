const models = require('../models');
var path = require('path');

const constants = require('../config/appConstants');

const fileExtension = require('file-extension');
const util = require('util');
const sharp = require('sharp') //for image thumbnail
const Thumbler = require('thumbler');//for video thumbnail
const fs = require('fs-extra')
const crypto = require('crypto');
const Models = require('../models');
/*
|----------------------------------------------------------------------------------------------------------------------------
|   Exporting all methods
|----------------------------------------------------------------------------------------------------------------------------
*/
module.exports = {

    readFile: async (path) => {
        console.log("  ************ readFile *******************")
        console.log(path, "  ************ pathreadFile *******************")
        return new Promise((resolve, reject) => {
            const readFile = util.promisify(fs.readFile);
            readFile(path).then((buffer) => {
                resolve(buffer);
            }).catch((error) => {
                reject(error);
            });
        });
    },

    writeFile: async (path, buffer) => {
        console.log("  ************ write file *******************")
        return new Promise((resolve, reject) => {
            const writeFile1 = util.promisify(fs.writeFile);
            writeFile1(path, buffer).then((buffer) => {
                resolve(buffer);
            }).catch((error) => {
                reject(error);
            });
        });
    },

    createVideoThumb: async (fileData, thumbnailPath) => {
        var VIDEO_THUMBNAIL_TIME = '00:00:02'
        var VIDEO_THUMBNAIL_SIZE = '300x200'
        var VIDEO_THUMBNAIL_TYPE = 'video'
        return new Promise(async (resolve, reject) => {
            Thumbler({
                type: VIDEO_THUMBNAIL_TYPE,
                input: fileData,
                output: thumbnailPath,
                time: VIDEO_THUMBNAIL_TIME,
                size: VIDEO_THUMBNAIL_SIZE // this optional if null will use the desimention of the video
            }, function (err, path) {
                if (err) reject(err);
                resolve(path);
            });
        });

    },

    fileUploadMultiparty: async function (FILE, FOLDER, FILE_TYPE) {
        try {
            var FILENAME = FILE.name; // actual filename of file
            var FILEPATH = FILE.tempFilePath; // will be put into a temp directory

            THUMBNAIL_IMAGE_SIZE = 300
            THUMBNAIL_IMAGE_QUALITY = 100

            let EXT = fileExtension(FILENAME); //get extension
            EXT = EXT ? EXT : 'jpg';
            FOLDER_PATH = FOLDER ? (FOLDER + "/") : ""; // if folder name then add following "/" 
            var ORIGINAL_FILE_UPLOAD_PATH = "/uploads/" + FOLDER_PATH;
            var THUMBNAIL_FILE_UPLOAD_PATH = "/uploads/" + FOLDER_PATH;
            var NEW_FILE_NAME = (new Date()).getTime() + "-" + "file." + EXT;
            var NEW_THUMBNAIL_NAME = (new Date()).getTime() + "-" + "thumbnail" + "-" + "file." + ((FILE_TYPE == "video") ? "jpg" : EXT);

            let NEWPATH = path.join(__dirname, '../public/', ORIGINAL_FILE_UPLOAD_PATH, NEW_FILE_NAME);
            let THUMBNAIL_PATH = path.join(__dirname, '../public/', ORIGINAL_FILE_UPLOAD_PATH, NEW_THUMBNAIL_NAME);

            let FILE_OBJECT = {
                "image": '',
                "thumbnail": '',
                "fileName": FILENAME,
                "folder": FOLDER,
                "file_type": FILE_TYPE
            }

            let BUFFER = await this.readFile(FILEPATH); //read file from temp path
            await this.writeFile(NEWPATH, BUFFER); //write file to destination

            FILE_OBJECT.image = THUMBNAIL_FILE_UPLOAD_PATH + NEW_FILE_NAME;

            let THUMB_BUFFER = "";

            if (FILE_TYPE == 'image') { // image thumbnail code
                var THUMB_IMAGE_TYPE = (EXT == "png") ? "png" : "jpeg";
                THUMB_BUFFER = await sharp(BUFFER)
                    .resize(THUMBNAIL_IMAGE_SIZE)
                    .toFormat(THUMB_IMAGE_TYPE, {
                        quality: THUMBNAIL_IMAGE_QUALITY
                    })
                    .toBuffer();

                FILE_OBJECT.thumbnail = THUMBNAIL_FILE_UPLOAD_PATH + NEW_THUMBNAIL_NAME;
                await this.writeFile(THUMBNAIL_PATH, THUMB_BUFFER);
            } else if (FILE_TYPE == "video") { // video thumbnail code
                await this.createVideoThumb(NEWPATH, THUMBNAIL_PATH, NEW_THUMBNAIL_NAME);
                FILE_OBJECT.thumbnail = THUMBNAIL_FILE_UPLOAD_PATH + NEW_THUMBNAIL_NAME;
            }
            else{
                FILE_OBJECT.thumbnail = ''
            }
            return FILE_OBJECT;
        } catch (err) {
            console.log(err);
            throw err;
        }
    },

    error: function (res, err, req) {
        console.log(err, '===========================>error');
        let code = (typeof err === 'object') ? (err.code) ? err.code : 403 : 403;
        let message = (typeof err === 'object') ? (err.message ? err.message : '') : err;
      
        if (req) {
            req.flash('flashMessage', {
                color: 'error',
                message
            });

            const originalUrl = req.originalUrl.split('/')[1];
            return res.redirect(`/${originalUrl}`);
        }

        return res.status(code).json({
            'success': false,
            'message': message,
            'code': code,
            'body': {}
        });

    },
    success: function (res, message = '', body = {}) {
        return res.status(200).json({
            'success': true,
            'code': 200,
            'message': message,
            'body': body
        });
    },

    failed: function (res, message = '') {
        message = (typeof message === 'object') ? (message.message ? message.message : '') : message;
        return res.status(400).json({
            'success': false,
            'code': 400,
            'message': message,
            'body': {}
        });
    },

}