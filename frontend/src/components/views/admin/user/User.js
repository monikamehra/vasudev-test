import React, {useState, useEffect} from "react";
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import * as Path from 'routes/paths';
import AuthService from 'services';
import swal from 'sweetalert';
import Pagination from "react-js-pagination";
import { List } from 'react-content-loader';
import env from 'env.json';

const User = (props) => {

    //pagination
    const [ activePage, setActivePage ] = useState(1);
    const [totalItemsCount, setTotalItemsCount] = useState(0);
    const [itemsCountPerPage, setItemsCountPerPage] = useState(0);
    const [pageRange, setPageRange] = useState(5);
 
    const [sending, setSending] = useState(false);
    const [load, setLoad] = useState(false);
    const [users, setUsers] = useState([]);
    const MODULE_NAME = 'user';

    //search record
    const [sortby, setSortby] = useState(1);
    const [ values, setValues] = useState({
        search:"",
    });

    //handle change
    const handleChange = (e) => {
        const { name, value } = e.target;
        setValues({
            ...values,
            [name]: value
        });
    }; 

    //get user
    let isMounted = true;
    async function getData() {
        try{
            let query = {
                page: activePage,
                MODULE_NAME,
                sortby,
                search:values.search
            };
            setSending(true);
            await props.dispatch(AuthService.getList(query)).then((res) => {
                if(isMounted){
                   setSending(false);
                   setUsers(res.body.data);
                   setItemsCountPerPage(res.body.per_page);
                   setTotalItemsCount(res.body.total_count);
                }  
            });
        }catch(err) {
            setSending(false);
            console.log(err);
            if(err && err.data && err.data.message){
                swal("Oops!", err.data.message, "error");
            }
        }
    };

    useEffect(() => {
        getData();
        return () => {
            isMounted = false;
        };
    }, [activePage, sortby]);

    //update user status
    const updateStatus = async (id) => {
        try{
            let query = {
                id: id,
                MODULE_NAME
            };
            await props.dispatch(AuthService.updateStatus(query)).then((res) => {
                swal("Success!", res.message, "success");
                getData();
            });
        }catch(err) {
            console.log(err);
            setSending(false);
            if(err && err.data && err.data.message){
                swal("Oops!", err.data.message, "error");
            }
        }
    }

    //delete users
	const popupDelete = id =>{
		try {
			swal({
				title: "Are you sure?",
				text: "Are you sure to Delete?",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
			.then(willDelete => {
				if (willDelete) {
                    try{
                        let query = {
                            id: id,
                            MODULE_NAME
                        };
                        props.dispatch(AuthService.deleteData(query)).then((res) => {
                            swal("Deleted!", res.message, "success");
                            getData();
                        });
                    }catch(err) {
                        console.log(err);
                        setSending(false);
                        if(err && err.data && err.data.message){
                            swal("Oops!", err.data.message, "error");
                        }
                    }
                }
			});
		}
		catch(err) {
            console.log(err);
			swal("Oops!", "Something went wrong!", "error");
	    }
	}

    const handlePageChange = (page) => {
        setActivePage(page);
       window.scrollTo(0,0);
    }

    const handleSubmit_ = async (e) => {
        try{
            e.preventDefault();
            let query = {
                page: activePage,
                MODULE_NAME,
                sortby,
                search:values.search
            };
            setLoad(true);
            setSending(true);
            await props.dispatch(AuthService.getList(query)).then((res) => {
                setLoad(false);
                setSending(false);
                setUsers(res.body.data);
                setValues({
                    search:""
                })
            });
        }catch(err) {
            console.log(err);
            setSending(false);
            if(err && err.data && err.data.message){
                swal("Oops!", err.data.message, "error");
            }
        }
    }

    return(
        <>
        <Helmet title="Users" />
            <section className="content-header">
                <div className="container-fluid">
                    <div className="row mb-2">
                        <div className="col-sm-6">
                            <h1>Users</h1>
                        </div>
                    </div>
                </div>
            </section>
            <div className="card">
                <div className="card-header">
                    <div className="row">
                        <div className="col-md-6">
                            <h3 className="card-title">Users List</h3>
                        </div>
                        
                    </div>
                </div>
                
                <div className="card-body p-0">
                {sending &&
                    <List  
                        style={{ width: '100%', padding: 10, }} 
                        speed={2}
                        height={150}
                        backgroundColor="#f3f3f3"
                        foregroundColor="#c1c5c7"
                        viewBox="30 0 380 30"
                    />
                } 
                
                {!sending && users.length>0 &&
                    <table className="table table-striped projects">
                        <thead>
                            <tr>
                                <th style={{ width: '20%'}}>
                                    Image
                                </th>
                                <th style={{ width: '20%'}}>
                                    First Name
                                </th>
                                <th style={{ width: '20%'}}>
                                    Last Name
                                </th>
                                <th style={{ width: '30%'}}>
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        {users.length>0  && users.map((item, i) => {
                            console.log(item);
                            return(
                                <tr key={i}>
                                    <td>
                                        <a href={item.profile_picture.original ? env.LOCAL_URL+item.profile_picture.original : '#'} target="_blank">
                                        <img alt="Avatar" class="table-avatar profile-image-size"  src={item.profile_picture && item.profile_picture.original ? env.LOCAL_URL+item.profile_picture.thumbnail : '/assets/images/dummy.png'} />
                                        </a>
                                    </td>
                                    <td>{item.first_name} </td>
                                    <td>{item.last_name}</td>
                                    <td className="project-actions">
                                        <Link to={`${Path.userview}/${item._id}`} className="btn btn-primary btn-sm" title="View">
                                            <i className="fas fa-eye"></i>
                                        </Link>
                                        <span style={{ marginLeft:'10px'}} className="btn btn-danger btn-sm" title="Delete" onClick={() => {popupDelete(item._id)}}>
                                            <i className="fas fa-trash"></i>
                                        </span>
                                    </td>
                                </tr>
                            )
                        })}
                        </tbody>
                        <div className="pagination-box-review">
                            {(totalItemsCount>itemsCountPerPage) &&
                            <Pagination
                                activePage={activePage}
                                itemsCountPerPage={itemsCountPerPage}
                                totalItemsCount={totalItemsCount}
                                pageRangeDisplayed={pageRange}
                                onChange={(page) => handlePageChange(page)}
                                itemClass='page-item'
                                linkClass='page-link'
                            />
                            }
                        </div>
                    </table>
                }

                {(!sending && users.length==0) &&
                    <div className="row">
                        <div className="col-md-6 not-found-detail">
                            <h6>Data not found....</h6>
                        </div>
                    </div>
                }   
                </div>
            </div>
        </>
    )
};

const mapStateToProps = (state) => {
    return{
        isAuthenticated: state.Auth.isAuthenticated,
        user: state.Auth,
    }
};

function mapDispatchToProps(dispatch) {
    return { dispatch };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(User);

