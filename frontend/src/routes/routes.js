import * as Path from './paths';
//------------User--------------------------//
import User from 'components/views/admin/user/User';
import UserView from 'components/views/admin/user/UserView';

//---------------Page not found--------------//
import NotFound from 'components/NotFound';

const routes = [  
	{
		path: Path.user,
		exact: true,
		auth: false,
		component: User,
	},
	{
		path: Path.userview,
		exact: true,
		auth: false,
		component: UserView,
	},
	{
		path: Path.user_view,
		exact: true,
		auth: false,
		component: UserView,
	},
	{
		path: '/',
		exact: false,
		component: NotFound,
	},

];

export default routes;
